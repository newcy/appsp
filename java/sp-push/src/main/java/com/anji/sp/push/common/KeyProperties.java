package com.anji.sp.push.common;

import java.util.ResourceBundle;

public class KeyProperties {

    public static final ResourceBundle resourceBundle = ResourceBundle.getBundle("AJPushUrl");
//    public static final String app_key = resourceBundle.getString("app_key");
//    public static final String appid_hw = resourceBundle.getString("appid_hw");
//    public static final String appsecret_hw = resourceBundle.getString("appsecret_hw");
    public static final String token_server_hw = resourceBundle.getString("token_server_hw");
    public static final String push_open_url_hw = resourceBundle.getString("push_open_url_hw");
//
//    public static final String package_name_xm = resourceBundle.getString("package_name_xm");
//    public static final String security_xm = resourceBundle.getString("security_xm");
//
//    public static final String mastersecret_op = resourceBundle.getString("mastersecret_op");
//    public static final String appkey_op = resourceBundle.getString("appkey_op");
//
//    public static final String secret_vo = resourceBundle.getString("secret_vo");
//    public static final String appkey_vo = resourceBundle.getString("appkey_vo");
//    public static final String appid_vo = resourceBundle.getString("appid_vo");
//
//    public static final String appkey_jg = resourceBundle.getString("appkey_jg");
//    public static final String mastersecret_jg = resourceBundle.getString("mastersecret_jg");
}
