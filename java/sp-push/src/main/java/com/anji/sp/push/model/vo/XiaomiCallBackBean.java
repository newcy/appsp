package com.anji.sp.push.model.vo;

import java.io.Serializable;

/**
 * 小米推送回调Bean
 */
public class XiaomiCallBackBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private String param;
    private String targets;//"regId1,regId2,regId3"
    private String type;
    private String jobkey;
    private String barStatus;
    private String timeStamp;
    private String errorCode;
    private ReplaceTargetBean replaceTarget;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJobkey() {
        return jobkey;
    }

    public void setJobkey(String jobkey) {
        this.jobkey = jobkey;
    }

    public String getBarStatus() {
        return barStatus;
    }

    public void setBarStatus(String barStatus) {
        this.barStatus = barStatus;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ReplaceTargetBean getReplaceTarget() {
        return replaceTarget;
    }

    public void setReplaceTarget(ReplaceTargetBean replaceTarget) {
        this.replaceTarget = replaceTarget;
    }

    class ReplaceTargetBean {
        private String regId1;

        public String getRegId1() {
            return regId1;
        }

        public void setRegId1(String regId1) {
            this.regId1 = regId1;
        }
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

/**
 * {
 *       //type为1表示该消息已送达，targets为送达的设备alias列表
 *        "msgId1":{"param":"param","type": 1, "targets":"alias1,alias2,alias3", "jobkey": "123" ,"barStatus":"Enable","timeStamp":1324167800000},
 *
 *       //type为2表示该消息被点击，targets为点击的设备alias列表
 *       "msgId2":{"param":"param","type": 2, "targets":"alias1,alias2,alias3", "jobkey": "456", "barStatus": "Enable", "timeStamp": 1524187800000},
 *
 *       //type为16表示目标设备无效，targets为无效的alias列表
 *       "msgId3":{"param":"param","type":16,"targets":"alias1,alias2,alias3","barStatus":"Unknown","timestamp":xxx},
 *
 *       //type为16表示目标设备无效，targets为无效的regId列表，errorCode为1表示无效regId
 *       "msgId4":{"param":"param","type":16,"targets":"regId1,regId2,regId3","barStatus":"Unknown","errorCode":1,"timestamp":xxx,"replaceTarget":{"regId1":"otherRegId"}},
 *
 *        //type为64表示目标设备不符合过滤条件
 *       "msgId5":{"param":"param","type":64,"targets":"regId1,regId2,regId3", "barStatus":"Unknown","timestamp":1572228055643}
 *
 *        //type为128表示当日推送总量超限
 *       "msgId6": {"extra":{"ack":"当日已送达数","quota":"当日可以下发总数"},"type":128,"targets":"alias","timestamp":1585203103625}
 *
 * }
 */
}
