package com.anji.sp.push.service;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.po.PushConfiguresPO;
import com.anji.sp.push.model.vo.RequestSendBean;

public interface PushService {
    /**
     * 批量发送
     *
     * @param requestSendBean
     * @param pushConfiguresPO
     * @param msgId
     * @param targetType
     * @return
     */
    ResponseModel pushBatchMessage(RequestSendBean requestSendBean, PushConfiguresPO pushConfiguresPO, String msgId, String targetType) throws Exception;

    /**
     * 全发
     *
     * @param requestSendBean
     * @param pushConfiguresPO
     * @param msgId
     * @param targetType
     * @return
     */
    ResponseModel pushAllMessage(RequestSendBean requestSendBean, PushConfiguresPO pushConfiguresPO, String msgId, String targetType) throws Exception;
}
