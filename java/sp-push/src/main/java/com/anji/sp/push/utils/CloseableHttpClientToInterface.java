package com.anji.sp.push.utils;

import com.alibaba.fastjson.JSONObject;
import com.anji.sp.push.model.vo.PushMessageVO;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @Author: Kean
 * @Date: 2021/2/20
 * @Description:
 * Apache封装好的CloseableHttpClient
 *
 *          <dependency>
 *             <groupId>org.apache.httpcomponents</groupId>
 *             <artifactId>httpclient</artifactId>
 *             <version>4.5.12</version>
 *         </dependency>
 */
public class CloseableHttpClientToInterface {
    private static CloseableHttpClient httpClient = null;


    /**
     * 以post方式调用第三方接口
     * @param url
     * @param data
     * @return
     */
    public static String doPost(String url, String data){

        try {
            if (httpClient == null){
                httpClient = HttpClientBuilder.create().build();
            }

            HttpPost post = new HttpPost(url);

            //api_gateway_auth_token自定义header头，用于token验证使用
//            post.addHeader("token", tokenString);
            post.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36");

            StringEntity s = new StringEntity(data);
            s.setContentEncoding("UTF-8");
            //发送json数据需要设置contentType
            s.setContentType("application/json;charset=utf-8");
            //设置请求参数
            post.setEntity(s);
            HttpResponse response = httpClient.execute(post);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
                //返回json格式
                String res = EntityUtils.toString(response.getEntity());
                return res;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (httpClient != null){
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        postPushBath();
    }


    public static void getMessage() {
        PushMessageVO vo = new PushMessageVO();
        vo.setAppKey("fb90cb5bc0c84a50883a3a2cc9295fbf");
        vo.setMsgId("812808455583072256");
        String data = JSONObject.toJSONString(vo);

        String s = doPost("http://open-appsp.anji-plus.com/sp/push/queryHistoryByAppKeyAndMsgId", data);
        System.out.println(s);
    }
    public static void postPushBath() {
        AJPushMessage ajPushMessage = new AJPushMessage();
        ajPushMessage.setAppKey("fae48dd5c3834ff4aac9a12e79b0b481");
        ajPushMessage.setSecretKey("55c773aee67245189a4dc98d5a46c1ab");
        ajPushMessage.setContent("第三方调用测试1234");
        ajPushMessage.setTitle("第三方调用测试333323");

        ArrayList<String> deviceIds = new ArrayList<>();
        deviceIds.add("13165ffa4ea9462aecf");
        ajPushMessage.setDeviceIds(deviceIds);

        HashMap<String, String> extras = new HashMap<>();
        extras.put("test", "1231241243");
        ajPushMessage.setExtras(extras);

        ajPushMessage.setPushType("0");

        HashMap<String, Object> sound = new HashMap<>();
        sound.put("sound", "aa");
        ajPushMessage.setAndroidConfig(sound);

        HashMap<String, Object> soundIOS = new HashMap<>();
        soundIOS.put("sound", "aa.caf");
        ajPushMessage.setIosConfig(soundIOS);

        String data = JSONObject.toJSONString(ajPushMessage);

        String s = doPost("http://appsp.dev.anji-plus.com/sp/push/pushBatch", data);
//        String s = doPost("http://localhost:8081/sp/push/pushBatch", data);
        System.out.println(s);
    }

}
