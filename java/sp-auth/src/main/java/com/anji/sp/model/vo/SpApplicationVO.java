package com.anji.sp.model.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 应用
 *
 * @author kean 2020-06-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("应用表")
public class SpApplicationVO extends BaseVO implements Serializable {

    @ApiModelProperty("app_id")
    private Long appId;

    @ApiModelProperty("应用唯一key")
    private String appKey;

    @ApiModelProperty("应用名称")
    private String name;

    @ApiModelProperty("公钥")
    private String publicKey;

    @ApiModelProperty("logo Url")
    private String logoUrl;
    @ApiModelProperty("推广版本/环境信息")
    private String promoteVersion;
    @ApiModelProperty("推广应用名")
    private String promoteName;
    @ApiModelProperty("推广语")
    private String promoteDesc;
    @ApiModelProperty("推广介绍")
    private String promoteIntroduction;

    @ApiModelProperty("是否可编辑")
    private boolean enable;
}