package com.anji.sp.mapper;

import com.anji.sp.model.po.SpFilePO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色
 *
 * @author Kean 2020-06-23
 */
public interface SpFileMapper extends BaseMapper<SpFilePO> {
}