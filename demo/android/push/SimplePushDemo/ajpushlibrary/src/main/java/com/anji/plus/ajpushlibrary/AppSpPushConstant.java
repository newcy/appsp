package com.anji.plus.ajpushlibrary;


/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.d
 * <p>
 * 在平台上开启服务成功后获取AppId、Appkey和AppSecret
 * </p>
 */

public class AppSpPushConstant {
    public static final String HNOTIFICATIONMESSAGE = "notificationMessage";
    public static final String OTHER = "0";
    public static final String HW = "1";
    public static final String XM = "2";
    public static final String OPPO = "3";
    public static final String VIVO = "4";

    public static final String MESSAGE_RECEIVED_ACTION = "com.anji.plus.ajpushlibrary.PushMessageReceiver";
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_EXTRAS = "extras";

    public static String jPushRegId = "jPushRegId";//极光唯一标识,regID，当做存储的key

    public static String brandType = "";//手机品牌
    public static String packageName = "";//包名
    public static String pushToken = "";//四大厂商注册推送服务成功后生成的唯一标识token

    public static String appspHost = "";
    public static String appspAppKey = "a1b49e089de04915b124ebdec715761d";
    public static String appspSecretKey = "e7ded5c2d4934d6683e72f6d9fde2b4d";
    public static String oppoAppKey = "c160669462604212962064ffa2df36af";
    public static String oppoAppSecret = "b77b1509f99043e3b695795366e929ef";
    public static String xmAppId = "2882303761518880022";
    public static String xmAppKey = "5161888025022";
}
