package com.anji.plus.ajpushlibrary.jpush;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.anji.plus.ajpushlibrary.AppSpPushConfig;
import com.anji.plus.ajpushlibrary.AppSpPushLog;
import com.anji.plus.ajpushlibrary.AppSpPushConstant;
import com.anji.plus.ajpushlibrary.model.AppSpModel;
import com.anji.plus.ajpushlibrary.service.IAppSpCallback;
import com.anji.plus.ajpushlibrary.util.MessageUtil;
import com.anji.plus.ajpushlibrary.util.SPUtil;

import cn.jpush.android.api.CmdMessage;
import cn.jpush.android.api.CustomMessage;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.JPushMessage;
import cn.jpush.android.api.NotificationMessage;
import cn.jpush.android.service.JPushMessageReceiver;

/**
 * 用户自定义接收消息器,3.0.7开始支持,目前新tag/alias接口设置结果会在该广播接收器对应的方法中回调
 */
public class PushMessageReceiver extends JPushMessageReceiver {

    @Override
    public void onMessage(Context context, CustomMessage customMessage) {
        AppSpPushLog.e("[onMessage] " + customMessage);
        //获取透传信息
        processCustomMessage(context, customMessage);
    }

    @Override
    public void onNotifyMessageOpened(Context context, NotificationMessage message) {
        AppSpPushLog.e("[onNotifyMessageOpened] " + message);
        // 点击通知栏消息，发送广播给打开自定义页面
//        notificationMessageModel.setMessageType(3);//小米极光点击通知栏跳转页面
        MessageUtil.onMessageClicked(context, message.notificationTitle, message.notificationContent, message.notificationExtras);
    }

    @Override
    public void onMultiActionClicked(Context context, Intent intent) {
        AppSpPushLog.e("[onMultiActionClicked] 用户点击了通知栏按钮");
        String nActionExtra = intent.getExtras().getString(JPushInterface.EXTRA_NOTIFICATION_ACTION_EXTRA);

        //开发者根据不同 Action 携带的 extra 字段来分配不同的动作。
        if (nActionExtra == null) {
            AppSpPushLog.d("ACTION_NOTIFICATION_CLICK_ACTION nActionExtra is null");
            return;
        }
        if (nActionExtra.equals("my_extra1")) {
            AppSpPushLog.e("[onMultiActionClicked] 用户点击通知栏按钮一");
        } else if (nActionExtra.equals("my_extra2")) {
            AppSpPushLog.e("[onMultiActionClicked] 用户点击通知栏按钮二");
        } else if (nActionExtra.equals("my_extra3")) {
            AppSpPushLog.e("[onMultiActionClicked] 用户点击通知栏按钮三");
        } else {
            AppSpPushLog.e("[onMultiActionClicked] 用户点击通知栏按钮未定义");
        }
    }

    @Override
    public void onNotifyMessageArrived(Context context, NotificationMessage message) {
        AppSpPushLog.e("[onNotifyMessageArrived] " + message);
        // 消息到达时，将声音信息发给接收声音信息的广播
//        notificationMessageModel.setMessageType(2);//极光通知栏声音
        MessageUtil.onMessageArrived(context, message.msgId, message.notificationTitle, message.notificationContent, message.notificationExtras);
    }

    @Override
    public void onNotifyMessageDismiss(Context context, NotificationMessage message) {
        AppSpPushLog.e("[onNotifyMessageDismiss] " + message);
    }

    @Override
    public void onRegister(Context context, String registrationId) {
        AppSpPushLog.i("[onRegister] " + registrationId);

        Log.i("极光回调结果", registrationId);
        //將极光的registrationId保存再sp中
        SPUtil.put(context, AppSpPushConstant.jPushRegId, registrationId);
//        AppSpPushConstant.jPushRegId = registrationId;
        //send the Registration Id to your server...
        AppSpPushConfig.getInstance().sendRegTokenToServer(new IAppSpCallback() {
            @Override
            public void pushInfo(AppSpModel<String> appSpModel) {

            }

            @Override
            public void error(String code, String msg) {

            }
        });

    }

    @Override
    public void onConnected(Context context, boolean isConnected) {
        AppSpPushLog.i("[onConnected] " + isConnected);
    }

    @Override
    public void onCommandResult(Context context, CmdMessage cmdMessage) {
        AppSpPushLog.i("[onCommandResult] " + cmdMessage);
    }

    @Override
    public void onTagOperatorResult(Context context, JPushMessage jPushMessage) {
        TagAliasOperatorHelper.getInstance().onTagOperatorResult(context, jPushMessage);
        super.onTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onCheckTagOperatorResult(Context context, JPushMessage jPushMessage) {
        TagAliasOperatorHelper.getInstance().onCheckTagOperatorResult(context, jPushMessage);
        super.onCheckTagOperatorResult(context, jPushMessage);
    }

    @Override
    public void onAliasOperatorResult(Context context, JPushMessage jPushMessage) {
        TagAliasOperatorHelper.getInstance().onAliasOperatorResult(context, jPushMessage);
        super.onAliasOperatorResult(context, jPushMessage);
    }

    @Override
    public void onMobileNumberOperatorResult(Context context, JPushMessage jPushMessage) {
        TagAliasOperatorHelper.getInstance().onMobileNumberOperatorResult(context, jPushMessage);
        super.onMobileNumberOperatorResult(context, jPushMessage);
    }

    //send msg to MainActivity
    private void processCustomMessage(Context context, CustomMessage customMessage) {
        //获取透传信息后将获取的数据通过广播形式发送
//        notificationMessageModel.setMessageType(1);//极光透传
        MessageUtil.onMessageArrived(context, customMessage.messageId, customMessage.title, customMessage.message, customMessage.extra);
    }

    @Override
    public void onNotificationSettingsCheck(Context context, boolean isOn, int source) {
        super.onNotificationSettingsCheck(context, isOn, source);
        AppSpPushLog.e("[onNotificationSettingsCheck] isOn:" + isOn + ",source:" + source);
    }

}
