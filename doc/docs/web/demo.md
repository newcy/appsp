# 前端手册

## 二级目录
123123
## 二级目录
22222
### 三级目录

#### 本地图片
![本地图片](../assets/test.jpg)

#### 网络图片
![网络图片](https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1554959141,1013062531&fm=26&gp=0.jpg)

#### 表格
### 属性说明
| 属性        | 类型   |  默认  |  备注 |
| --------   | :-----:  | :----:  | :----:  |
| text      | String  |   -    | 标题 |
| detail    | String  |   -   | 详情 |
| show | Boolean/String | true | 是否显示 |

### 代码片段
#### JS
```javascript
import adCell from '@/node_modules/adcell/ADCell.vue';
export default {
    components: {adCell}
}
```
#### html

```html
<adCell text="普通"></adCell>
<adCell text="点击事件" @click="onClick"></adCell>
<adCell text="隐藏箭头" showArrow="false"></adCell>
```

#### Java

```java
class Javahelloworld {
    public static void main(String args[]){
        System.out.println("hello world\n");
    }
}
```

#### OC

```objc
@interface ClassName : NSObject

+(void)method: (NSString *)text success:(SuccessCallBack)success;

@end
```

#### Swift

```swift
import UIKit
import HandyJSON

class TestModel: BaseModel {
    var repData: SubModel!
    
}
```